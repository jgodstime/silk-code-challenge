<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\ProgrammeTimetableController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('channels')->group(function () {

    // Channel
    Route::get('/', [ChannelController::class, 'list']);
    Route::post('/', [ChannelController::class, 'create']);
    Route::get('/{channelId}/show', [ChannelController::class, 'show']);
    Route::delete('/{channelId}', [ChannelController::class, 'delete']);
    Route::put('/{channelId}', [ChannelController::class, 'update']);


    // Programme
    Route::post('{channelId}/programs/', [ProgrammeTimetableController::class, 'create']);

    Route::put('/{channelId}/programs/{programeId}', [ProgrammeTimetableController::class, 'update']);

    Route::get('/{channelId}/{date}', [ProgrammeTimetableController::class, 'listAfterDay']);


});
