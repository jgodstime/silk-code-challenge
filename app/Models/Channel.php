<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Channel extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'icon_path',
        'is_active'
    ];

    public function getIconPathAttribute($value)
    {
        return url('storage/'.$value);
    }

    public function scopeOfName($query, $name)
    {
        return $query->orWhere('name', 'like', "%{$name}%");
    }

    public function scopeOfIsActive($query, $value)
    {
        return $query->orWhere('is_active', $value);
    }

    /**
     * Get all of the comments for the Channel
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pragrammeTimetables()
    {
        return $this->hasMany(ProgrammeTimetable::class);
    }


}
