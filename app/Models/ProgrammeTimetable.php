<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgrammeTimetable extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'channel_id',
        'name',
        'start_time',
        'end_time',
        'duration' // duration is in seconds
    ];

    public function scopeOfName($query, $name)
    {
        return $query->orWhere('name', 'like', "%{$name}%");
    }

    /**
     * Get the channe; that owns the ProgrammeTimetable
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }
}
