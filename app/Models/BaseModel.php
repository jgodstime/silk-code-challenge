<?php
namespace App\Models;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use ModelUuid;

    // public $incrementing = false;

}
