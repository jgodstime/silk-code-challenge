<?php
namespace App\Http\Services;

use Carbon\Carbon;
use App\Models\Channel;
use Illuminate\Http\Request;
use App\Http\Services\BaseService;
use App\Models\ProgrammeTimetable;

class ProgrammeTimetableService extends BaseService{

    protected $programmeTimetable;

    public function __construct(){
        $this->programmeTimetable = new ProgrammeTimetable();
    }

    public function create($channelId, array $data)
    {
        if(!$channel = Channel::find($channelId)){
            return $this->fail("Channel not found", 404);
        }

        if($this->programmeTimetable->where('name', $data['name'])
                    ->where('channel_id', $channelId)
                    ->first())
        {
            return $this->fail("Programme with name {$data['name']} already exist in the selected channel", 409);
        }


        $startTime = Carbon::parse($data['start_time']);
        $endTime = Carbon::parse($data['end_time']);

        $diffInSeconds = $startTime->diffInSeconds($endTime);

        $data['channel_id'] = $channelId;
        $data['duration'] = $diffInSeconds;

        $data['start_time'] = $startTime;
        $data['end_time'] = $endTime;

        $programmeTimetable = $this->programmeTimetable->create($data);

        if(!$programmeTimetable){
            return $this->fail('Unable to create programmer timetable at this time, please try again', 500);
        }

        return $this->success($programmeTimetable);
    }

    public function listAfterDay($channelId, $date)
    {
        $orderBy = !empty($data['order_by']) ? $data['order_by'] : 'DESC';

        $programmeTimetables = $this->programmeTimetable->with('channel')->where('channel_id', $channelId)
                                    ->where('duration', '>', $date)
                                    ->orderBy('created_at',$orderBy);

        if(!empty($data['name'])){
            $programmeTimetables = $programmeTimetables->ofName($data['name']);
        }

        // dd($programmeTimetables->paginate());
        return $programmeTimetables->paginate();
    }

    public function delete(string $programmeTimetableId)
    {
        if(!$programmeTimetable = $this->programmeTimetables->find($programmeTimetableId)){
            return $this->fail('Pogramme not found', 404);
        }

        $programmeTimetable->delete();

        return $this->success($programmeTimetable);
    }

    public function update(string $channelId, $programmeTimetableId, array $data)
    {
        if(!$programmeTimetable = $this->programmeTimetable->where('id', $programmeTimetableId)
                                ->where('channel_id', $channelId)
                                ->first())
        {
            return $this->fail('Channel not found', 404);
        }

        $programmeTimetable->update($data);

        return $this->success($programmeTimetable->refresh());
    }

}
