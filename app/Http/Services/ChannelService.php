<?php
namespace App\Http\Services;

use App\Models\Channel;
use Illuminate\Http\Request;
use App\Http\Services\BaseService;

class ChannelService extends BaseService{

    protected $channel;

    public function __construct(){
        $this->channel = new Channel();
    }

    public function create(array $data)
    {
        if($this->channel->where('name', $data['name'])->first()){
            return $this->fail("Channel with name {$data['name']} already exist", 409);
        }

        if(!empty($data['icon'])){
            $data['icon_path'] = $data['icon']->store('icon_files','public');
        }

        $channel = $this->channel->create($data);

        if(!$channel){
            return $this->fail('Unable to create channel at this time, please try again', 500);
        }

        return $this->success($channel);
    }

    public function list(array $data)
    {
        $orderBy = !empty($data['order_by']) ? $data['order_by'] : 'DESC';

        $channels = $this->channel->with('pragrammeTimetables')->orderBy('created_at',$orderBy);

        if(!empty($data['name'])){
            $channels = $channels->ofName($data['name']);
        }

        if(!empty($data['is_active'])){
            $channels = $channels->ofName($data['is_active']);
        }

        return $channels->paginate();
    }

    public function show(string $channelId)
    {
        return $this->channel->find($channelId);
    }

    public function delete(string $channelId)
    {
        if(!$channel = $this->channel->find($channelId)){
            return $this->fail('Channel not found', 404);
        }

        $channel->delete();

        return $this->success($channel);
    }

    public function update(string $channelId, array $data)
    {
        if(!$channel = $this->channel->find($channelId)){
            return $this->fail('Channel not found', 404);
        }

        $channel->update($data);

        return $this->success($channel->refresh());
    }

}
