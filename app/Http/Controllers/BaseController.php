<?php
namespace App\Http\Controllers;

use App\Traits\FormatResponse;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
   use FormatResponse;
}
