<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\ChannelService;
use App\Http\Controllers\BaseController;
use App\Http\Requests\CreateChannelRequest;
use App\Http\Requests\UpdateChannelRequest;
use App\Http\Transformers\ChannelTransformer;

class ChannelController extends BaseController
{
    protected $channelService;
    protected $channelTransformer;

    public function __construct()
    {
        $this->channelService = new ChannelService();
        $this->channelTransformer = new ChannelTransformer();
    }

    public function create(CreateChannelRequest $request)
    {
        return $this->channelService->create($request->validated());
    }

    public function list(Request $request)
    {
        $channels = $this->channelService->list($request->all());
        return $this->transformPages($channels, $this->channelTransformer, 'channels');
    }

    public function show($channelId)
    {
        $channel = $this->channelService->show($channelId);
        if(!$channel){
            return $this->fail('Channel not found', 404);
        }
        return $this->transform($channel, $this->channelTransformer);
    }

    public function delete($channelId)
    {
        return $this->channelService->delete($channelId);
    }

    public function update($channelId, UpdateChannelRequest $request)
    {
        return $this->channelService->delete($channelId, $request->validated());
    }

}
