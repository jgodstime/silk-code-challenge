<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\ProgrammeTimetableService;
use App\Http\Requests\CreateProgrammeTimetableRequest;
use App\Http\Requests\UpdateProgrammeTimetableRequest;
use App\Http\Transformers\ProgrammeTimetableTransformer;

class ProgrammeTimetableController extends BaseController
{
    protected $programmeTimetableService;
    protected $programmeTimetableTransformer;

    public function __construct()
    {
        $this->programmeTimetableService = new ProgrammeTimetableService();
        $this->programmeTimetableTransformer = new ProgrammeTimetableTransformer();
    }

    public function create($channelId, CreateProgrammeTimetableRequest $request)
    {
        return $this->programmeTimetableService->create($channelId, $request->validated());
    }

    public function update($channelId, $programmeTimetableId, UpdateProgrammeTimetableRequest $request)
    {
        return $this->programmeTimetableService->update($channelId, $programmeTimetableId, $request->validated());
    }

    public function listAfterDay($channelId, $date)
    {
        if(!is_numeric($date)){
            return $this->fail('Date or last parameter must be an integer value');
        }

        $programmeTimetables = $this->programmeTimetableService->listAfterDay($channelId, $date);
        return $this->transformPages($programmeTimetables, $this->programmeTimetableTransformer, 'programme_timetables');
    }
    
}
