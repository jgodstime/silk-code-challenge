<?php
namespace App\Http\Transformers;

use App\Models\Channel;
use App\Models\ProgrammeTimetable;
use League\Fractal\TransformerAbstract;


class ProgrammeTimetableTransformer extends TransformerAbstract
{

    public function transform(ProgrammeTimetable $programe){
        return [
            'id' => $programe->id,
            'channel' => $programe->channel,
            'name' => $programe->name,
            'start_time' => $programe->start_time,
            'emd_time' => $programe->end_time,
            'duration' => $programe->duration,
           'created_at' => $programe->created_at,
        ];
    }
}
