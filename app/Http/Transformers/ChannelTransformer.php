<?php
namespace App\Http\Transformers;

use App\Models\Channel;
use League\Fractal\TransformerAbstract;


class channelTransformer extends TransformerAbstract
{

    public function transform(Channel $channel){
        return [
           'id' => $channel->id,
           'name' => $channel->name,
           'is_active' => $channel->is_active,
           'icon_path' => $channel->icon_path,
           'created_at' => $channel->created_at,
           'pragramme_timetables' => $channel->pragrammeTimetables
        ];
    }
}
