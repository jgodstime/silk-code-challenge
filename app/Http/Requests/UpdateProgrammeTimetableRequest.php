<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProgrammeTimetableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'channel_id' => 'nullable|exists:channels,id',
            'name' => 'nullable|string',
            'start_time' => 'nullable|date_format:h:i',
            'end_time' => 'nullable|date_format:h:i|after:start_time',
        ];
    }
}
