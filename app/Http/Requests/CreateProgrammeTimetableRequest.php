<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProgrammeTimetableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'channel_id' => 'required|exists:channels,id',
            'name' => 'required|string',
            'start_time' => 'required|date_format:h:i',
            'end_time' => 'required|date_format:h:i|after:start_time',
        ];
    }
}
