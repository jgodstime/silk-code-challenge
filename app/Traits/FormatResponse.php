<?php
namespace App\Traits;

use Illuminate\Support\Str;
// use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use League\Fractal\Serializer\ArraySerializer;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

trait FormatResponse
{
    use DispatchesJobs, ValidatesRequests, AuthorizesRequests;

   /**
     *
     * Format paginated data from a collection
     *
     * @param   [type]  $paginator    [$paginator description]
     * @param   [type]  $transformer  [$transformer description]
     *
     * @return  [type]                [return description]
     */
    protected function successWithPages($paginator, $transformer, $resourceName = null)
    {
        $collection = $paginator->getCollection();

        if (!$resourceName) {
            $resourceName = "items";
        }

        $data = fractal()
            ->collection($collection)
            ->transformWith($transformer)
            ->serializeWith(new ArraySerializer())
            ->withResourceName($resourceName)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json([
            'status' => "success",
            'data' => $data,
        ]);
    }

    protected function transformPages($paginator, $transformer, $resourceName = null)
    {
        $collection = $paginator->getCollection();

        if (!$resourceName) {
            $resourceName = "items";
        }

        return fractal()
            ->collection($collection)
            ->transformWith($transformer)
            ->serializeWith(new ArraySerializer())
            ->withResourceName($resourceName)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();


    }

    protected function transformPagesWithData($paginator, $transformer)
    {
        $collection = $paginator->getCollection();

        $data = fractal()
            ->collection($collection)
            ->transformWith($transformer)
            ->serializeWith(new ArraySerializer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();


            $data['status'] = "success";

            return response()->json($data);


    }

    protected function transform($model, $transformer)
    {
        $data = fractal($model, $transformer)->serializeWith(new \Spatie\Fractalistic\ArraySerializer());
        return $this->success($data);
    }

    /**
     * Format successful request response
     *
     * @param   [mixed]  $data  A string or array
     *
     * @return  [object]        JSON object
     */
    protected function success($data)
    {
        return response()->json(
            [
                'status' => "success",
                'data' => $data,
            ]
        );
    }

    protected function handleErrorResponse($response)
    {
        if (isset($response['error'])) {
            return $this->error($response['message'], $response['status_code']);
        }

        return $this->fail($response['message'], $response['status_code']);
    }

    /**
     * This handle formatted API error responses
     * @param $data
     * @param $code (optional) HTTP Error code
     */
    protected function error($data, $code = null)
    {
        if (!$code || is_string($code)) {
            $code = 422;
        }

        return response()->json([
            'status' => "error",
            'message' => $data,

        ], $code);
    }

    protected function fail($data, $code = null)
    {
        if (!$code || is_string($code)) {
            $code = 422;
        }

        return response()->json([
            'status' => "fail",
            'data' => $data,

        ], $code);
    }

     //this was created because the default pagination was not working for me
     public function customPaginate($items, $perPage = 15, $page = null, $options = [])
     {
         $options['endpoint'] =  url()->current();

         $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
         $items = $items instanceof Collection ? $items : Collection::make($items);
         return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
     }




}
